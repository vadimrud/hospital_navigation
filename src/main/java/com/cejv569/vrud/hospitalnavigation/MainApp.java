/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.vrud.hospitalnavigation;

import com.cejv569.vrud.hospitalnavigation.controller.HospitalFXMLController;
import com.cejv569.vrud.hospitalnavigation.persistence.HospitalDAO;
import com.cejv569.vrud.hospitalnavigation.persistence.HospitalDAOImpl;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vadim
 */
public class MainApp extends Application {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    private Stage primaryStage;

    private HospitalDAO hospitalDAO;

    /**
     * Constructor
     */
    public MainApp() {
        super();
        hospitalDAO = new HospitalDAOImpl();
    }

    /**
     * The application starts here
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        log.info("Program Begins");

        this.primaryStage = primaryStage;
        configureStage();
        
        primaryStage.setTitle(ResourceBundle.getBundle("messagesbundle").getString("Title"));
        primaryStage.show();
    }

    /**
     * Load the FXML and bundle, create a Scene and put the Scene on Stage.
     *
     * Using this approach allows you to use loader.getController() to get a
     * reference to the fxml's controller should you need to pass data to it.
     * Not used in this archetype.
     */
    private void configureStage() {
        try {
            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(MainApp.class.getResource("/fxml/hospitalFXML.fxml"));

            loader.setResources(ResourceBundle.getBundle("messagesbundle"));

            Parent parent = (AnchorPane) loader.load();

            Scene scene = new Scene(parent);

            primaryStage.setScene(scene);

            HospitalFXMLController controller = loader.getController();
            controller.setHospitalDAO(hospitalDAO);

        } catch (IOException | SQLException ex) { // getting resources or files
            // could fail
            log.error(null, ex);
            System.exit(1);
        }
    }

    /**
     * Where it all begins
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
