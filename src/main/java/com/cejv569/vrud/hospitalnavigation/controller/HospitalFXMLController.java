/**
 * Sample Skeleton for 'hospitalFXML.fxml' Controller Class
 */

package com.cejv569.vrud.hospitalnavigation.controller;

import com.cejv569.vrud.hospitalnavigation.MainApp;
import com.cejv569.vrud.hospitalnavigation.entities.Inpatient;
import com.cejv569.vrud.hospitalnavigation.entities.Patient;
import com.cejv569.vrud.hospitalnavigation.entities.Medication;
import com.cejv569.vrud.hospitalnavigation.entities.Surgical;
import com.cejv569.vrud.hospitalnavigation.inpatientController.InpatientFXMLController;
import com.cejv569.vrud.hospitalnavigation.medicationController.MedicationFXMLController;
import com.cejv569.vrud.hospitalnavigation.persistence.HospitalDAO;
import com.cejv569.vrud.hospitalnavigation.persistence.HospitalDAOImpl;
import com.cejv569.vrud.hospitalnavigation.surgicalController.SurgicalFXMLController;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HospitalFXMLController {
    
    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    
    private HospitalDAO hospitalDAO;
    private Patient patient;
    private InpatientFXMLController inpatientFXMLController;
    private MedicationFXMLController medicationFXMLController;
    private SurgicalFXMLController surgicalFXMLController;
    private Inpatient inpatient;
    private Medication medication;
    private Surgical surgical;
    private NumberFormat formatter;
    private Alert alertInfo;
    private Alert alertError;
    
    public HospitalFXMLController(){
        super();
        hospitalDAO = new HospitalDAOImpl();
        patient = new Patient();
        inpatient = new Inpatient();
        medication = new Medication();
        surgical = new Surgical();
        formatter = NumberFormat.getCurrencyInstance();
        alertInfo = new Alert(AlertType.INFORMATION);
        alertError = new Alert(AlertType.ERROR);
    }

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="prevBtn"
    private Button prevBtn; // Value injected by FXMLLoader

    @FXML // fx:id="searchBtn"
    private Button searchBtn; // Value injected by FXMLLoader

    @FXML // fx:id="nextBtn"
    private Button nextBtn; // Value injected by FXMLLoader

    @FXML // fx:id="clearBtn"
    private Button clearBtn; // Value injected by FXMLLoader

    @FXML // fx:id="saveBtn"
    private Button saveBtn; // Value injected by FXMLLoader

    @FXML // fx:id="deleteBtn"
    private Button deleteBtn; // Value injected by FXMLLoader

    @FXML // fx:id="reportBtn"
    private Button reportBtn; // Value injected by FXMLLoader

    @FXML // fx:id="exitBtn"
    private Button exitBtn; // Value injected by FXMLLoader

    @FXML // fx:id="mainPatientIDTextField"
    private TextField mainPatientIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="firstNameTextField"
    private TextField firstNameTextField; // Value injected by FXMLLoader

    @FXML // fx:id="lastNameTextField"
    private TextField lastNameTextField; // Value injected by FXMLLoader

    @FXML // fx:id="diagnosisTextField"
    private TextField diagnosisTextField; // Value injected by FXMLLoader

    @FXML // fx:id="admissionDateField"
    private DatePicker admissionDateField; // Value injected by FXMLLoader

    @FXML // fx:id="releaseDateField"
    private DatePicker releaseDateField; // Value injected by FXMLLoader
    
    @FXML // fx:id="inpatientPane"
    private AnchorPane inpatientPane; // Value injected by FXMLLoader

    @FXML // fx:id="medicationPane"
    private AnchorPane medicationPane; // Value injected by FXMLLoader

    @FXML // fx:id="surgicalPane"
    private AnchorPane surgicalPane; // Value injected by FXMLLoader

    @FXML
    void clearActionEvent(ActionEvent event) {
        patient.setPatientID(-1);
        lastNameTextField.setText("");
        firstNameTextField.setText("");
        admissionDateField.setValue(LocalDate.now());
        releaseDateField.setValue(LocalDate.now());
        diagnosisTextField.setText("");
    }

    @FXML
    void deleteActionEvent(ActionEvent event) throws SQLException {
        hospitalDAO.deletePatient(patient.getPatientID());
    }

    @FXML
    void exitActionEvent(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void nextActionEvent(ActionEvent event) throws SQLException {
        patient = hospitalDAO.findNextByID(patient);
    }

    @FXML
    void prevActionEvent(ActionEvent event) throws SQLException {
        patient = hospitalDAO.findPrevByID(patient);
    }

    @FXML
    void reportActionEvent(ActionEvent event) throws SQLException {
        double totalCost;
        totalCost = inpatientCost(patient.getPatientID()) + medicationCost(patient.getPatientID()) + surgicalCost(patient.getPatientID());
        alertInfo.setTitle("Patient Report");
        alertInfo.setHeaderText("Patient Report for: " + patient.getFirstName() + " " + patient.getLastName());
        alertInfo.setContentText("The total cost for the patient's stay is: " + formatter.format(totalCost));
        alertInfo.showAndWait();
    }

    @FXML
    void saveActionEvent(ActionEvent event) throws SQLException {
        
        if(areFieldsOK()){
            hospitalDAO.savePatient(patient);
        }
        
    }

    @FXML
    void searchActionEvent(ActionEvent event) throws SQLException {
        patient = hospitalDAO.findID(patient.getPatientID());
    }
    
    public void initMedicationPane(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(MainApp.class
                    .getResource("/fxml/medicationFXML.fxml"));
            AnchorPane tableView = (AnchorPane) loader.load();

            // Give the controller the data object.
            medicationFXMLController = loader.getController();
            medicationFXMLController.setHospitalDAO(hospitalDAO);
            medicationFXMLController.setPatient(patient);

            medicationPane.getChildren().add(tableView);
        } catch (IOException ex) {
            log.error(null, ex);
            Platform.exit();
        }
    }
    
    public void initInpatientPane(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(MainApp.class
                    .getResource("/fxml/inpatientFXML.fxml"));
            AnchorPane tableView = (AnchorPane) loader.load();

            // Give the controller the data object.
            inpatientFXMLController = loader.getController();
            inpatientFXMLController.setHospitalDAO(hospitalDAO);
            inpatientFXMLController.setPatient(patient);

            inpatientPane.getChildren().add(tableView);
        } catch (IOException ex) {
            log.error(null, ex);
            Platform.exit();
        }
    }
    
    public void initSurgicalPane(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(MainApp.class
                    .getResource("/fxml/surgicalFXML.fxml"));
            AnchorPane tableView = (AnchorPane) loader.load();

            // Give the controller the data object.
            surgicalFXMLController = loader.getController();
            surgicalFXMLController.setHospitalDAO(hospitalDAO);
            surgicalFXMLController.setPatient(patient);

            surgicalPane.getChildren().add(tableView);
        } catch (IOException ex) {
            log.error(null, ex);
            Platform.exit();
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        
        Bindings.bindBidirectional(lastNameTextField.textProperty(), patient.lastNameProperty());
        Bindings.bindBidirectional(firstNameTextField.textProperty(), patient.firstNameProperty());
        Bindings.bindBidirectional(diagnosisTextField.textProperty(), patient.diagnosisProperty());
        Bindings.bindBidirectional(admissionDateField.valueProperty(), patient.admissionDateProperty());
        Bindings.bindBidirectional(releaseDateField.valueProperty(), patient.releaseDateProperty());
        Bindings.bindBidirectional(mainPatientIDTextField.textProperty(), patient.patientIDProperty(), new NumberStringConverter());
        
        initInpatientPane();
        initMedicationPane();
        initSurgicalPane();

    }
    
    /**
     * this method calculates total cost of inpatient for given patient id
     * @param patientID
     * @return
     * @throws SQLException 
     */
    public double inpatientCost(int patientID) throws SQLException{
        
        double inpatientCost = 0;
        List<Inpatient> inpatientList = hospitalDAO.findID(patientID).getListOfInpatients();
        for(int i = 0; i < inpatientList.size(); i++){
            inpatientCost += inpatientList.get(i).getDailyRate();
            inpatientCost += inpatientList.get(i).getSupplies();
            inpatientCost += inpatientList.get(i).getServices();
        }
        
        return inpatientCost;
    }
    
    /**
     * this method calculates total cost of medication for given patient id
     * @param patientID
     * @return
     * @throws SQLException 
     */
    public double medicationCost(int patientID) throws SQLException{
        
        double medicationCost = 0;
        List<Medication> medicationList = hospitalDAO.findID(patientID).getListOfMedications();
        for(int i = 0; i < medicationList.size(); i++){
            medicationCost += medicationList.get(i).getUnitCost() * medicationList.get(i).getUnits();            
        }
        
        return medicationCost;
    }
    
    /**
     * this method calculates total cost of surgical for given patient id
     * @param patientID
     * @return
     * @throws SQLException 
     */
    public double surgicalCost(int patientID) throws SQLException{
        
        double surgicalCost = 0;
        List<Surgical> surgicalList = hospitalDAO.findID(patientID).getListOfSurgicals();
        for(int i = 0; i < surgicalList.size(); i++){
            surgicalCost += surgicalList.get(i).getRoomFee();
            surgicalCost += surgicalList.get(i).getSurgeonFee();
            surgicalCost += surgicalList.get(i).getSupplies();
        }
        
        return surgicalCost;
    }
    
    /**
     * this method is called to check if the strings entered are too long and
     * show an error if too long
     * @param input
     * @param type 
     * @return false if too long, true if ok
     */
    public boolean isStringOK(String input, String type){
        boolean isStringOK = true;
        if(input.length() > 255){
            alertError.setTitle("Input error has occured");
            alertError.setHeaderText("The " + type + " field is too long.");
            alertError.setContentText("This field must contain less than 256 characters");
            alertError.showAndWait();
            isStringOK = false;
        }
        return isStringOK;
    }
    
    /**
     * This method is called to check if double type entries are ok
     * @param input
     * @param type
     * @return true if ok, false if negative
     */
    public boolean isDoubleOK(double input, String type){
        boolean isDoubleOK = true;
        if(input < 0){
            alertError.setTitle("Input error has occured");
            alertError.setHeaderText(null);
            alertError.setContentText("The " + type + " field must be greater than 0.");
            alertError.showAndWait();
            isDoubleOK = false;
        }
        return isDoubleOK;
    }
    
    /**
     * this method is called to check if all fields are ok
     * @return true if ok, false if not ok
     */
    public boolean areFieldsOK(){
        boolean areFieldsOK = true;
        if((isStringOK(patient.getFirstName(), "First Name") == false)
                || (isStringOK(patient.getLastName(), "Last Name") == false)
                || (isStringOK(patient.getDiagnosis(), "Diagnosis") == false)){
            areFieldsOK = false;
        }
        return areFieldsOK;
    }
    
    /**
     * Sets a reference to the HospitalDAO object that retrieves data from the
     * database.
     * 
     * @param hospitalDAO
     * @throws SQLException 
     */
    public void setHospitalDAO(HospitalDAO hospitalDAO) throws SQLException {
        this.hospitalDAO = hospitalDAO;
        hospitalDAO.findNextByID(patient);
    }
}
