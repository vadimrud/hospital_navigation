/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.vrud.hospitalnavigation.entities;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author v_rudma
 */
public class Inpatient {
    
    private final IntegerProperty id;
    private final IntegerProperty patientID;
    private final ObjectProperty<LocalDate> dateOfStay;
    private final StringProperty roomNumber;
    private final DoubleProperty dailyRate;
    private final DoubleProperty supplies;
    private final DoubleProperty services;
    
    
    public Inpatient(){
        
        super();
        this.id = new SimpleIntegerProperty(-1);
        this.patientID = new SimpleIntegerProperty(-1);
        this.dateOfStay = new SimpleObjectProperty<>(LocalDate.now());
        this.roomNumber = new SimpleStringProperty("");
        this.dailyRate = new SimpleDoubleProperty(0.0);
        this.supplies = new SimpleDoubleProperty(0.0);
        this.services = new SimpleDoubleProperty(0.0);
        
    }
    
    public Inpatient(int id, int patientID, Timestamp dateOfStay, String roomNumber, double dailyRate, double supplies, double services){
        
        super();
        this.id = new SimpleIntegerProperty(id);
        this.patientID = new SimpleIntegerProperty(patientID);
        this.dateOfStay = new SimpleObjectProperty<>(dateOfStay.toLocalDateTime().toLocalDate());
        this.roomNumber = new SimpleStringProperty(roomNumber);
        this.dailyRate = new SimpleDoubleProperty(dailyRate);
        this.supplies = new SimpleDoubleProperty(supplies);
        this.services = new SimpleDoubleProperty(services);
        
    }    
    
    public int getId() {
	return id.get();
    }

    public void setId(final int id) {
	this.id.set(id);
    }

    public IntegerProperty idProperty() {
	return id;
    }
    
    public int getPatientID(){
        return patientID.get();
    }
    
    public void setPatientID(final int patientID){
        this.patientID.set(patientID);
    }
    
    public IntegerProperty patientIDProperty(){
        return patientID;
    }
    
    public Timestamp getDateOfStay() {
        return Timestamp.valueOf(dateOfStay.get().atStartOfDay());
    }
    
    public void setDateOfStay(final Timestamp dateOfStay) {
        this.dateOfStay.set(dateOfStay.toLocalDateTime().toLocalDate());
    }
    
    public ObjectProperty<LocalDate> dateOfStayProperty(){
        return dateOfStay;
    }
    
    public LocalDate getLocalDateDateOfStay(){
        return dateOfStay.get();
    }
    
    public void setLocalDateDateOfStay(LocalDate dateOfStay){
        this.dateOfStay.set(dateOfStay);
    }
    
    public String getRoomNumber() {
	return roomNumber.get();
    }

    public void setRoomNumber(final String roomNumber) {
	this.roomNumber.set(roomNumber);
    }

    public StringProperty roomNumberProperty() {
	return roomNumber;
    }
    
    public double getDailyRate() {
	return dailyRate.get();
    }

    public void setDailyRate(final double dailyRate) {
	this.dailyRate.set(dailyRate);
    }

    public DoubleProperty dailyRateProperty() {
	return dailyRate;
    }

    public double getSupplies() {
	return supplies.get();
    }

    public void setSupplies(final double supplies) {
	this.supplies.set(supplies);
    }

    public DoubleProperty suppliesProperty() {
	return supplies;
    }
    
    public double getServices() {
	return services.get();
    }

    public void setServices(final double services) {
	this.services.set(services);
    }

    public DoubleProperty servicesProperty() {
	return services;
    }

    @Override
    public String toString() {
        return "Inpatient" + "\nid= " + id 
                + "\npatientID= " + patientID 
                + "\ndateOfStay= " + dateOfStay 
                + "\nroomNumber= " + roomNumber 
                + "\ndailyRate= " + dailyRate 
                + "\nsupplies= " + supplies 
                + "\nservices= " + services;
    }
    
    
    
    
        
}
