/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.vrud.hospitalnavigation.entities;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author v_rudma
 */
public class Medication {

    private final IntegerProperty id;
    private final IntegerProperty patientID;
    private final ObjectProperty<LocalDate> dateOfMed;
    private final StringProperty med;
    private final DoubleProperty unitCost;
    private final DoubleProperty units;
    
    public Medication(){
        
        super();
        this.id = new SimpleIntegerProperty(-1);
        this.patientID = new SimpleIntegerProperty(-1);
        this.dateOfMed = new SimpleObjectProperty<>(LocalDate.now());
        this.med = new SimpleStringProperty("");
        this.unitCost = new SimpleDoubleProperty(0.0);
        this.units = new SimpleDoubleProperty(0.0);
        
    }
    
    public Medication(int id, int patientID, Timestamp dateOfMed, String med, double unitCost, double units){
        
        super();
        this.id = new SimpleIntegerProperty(id);
        this.patientID = new SimpleIntegerProperty(patientID);
        this.dateOfMed = new SimpleObjectProperty<>(dateOfMed.toLocalDateTime().toLocalDate());
        this.med = new SimpleStringProperty(med);
        this.unitCost = new SimpleDoubleProperty(unitCost);
        this.units = new SimpleDoubleProperty(units);
        
    }    
    
    public int getId() {
	return id.get();
    }

    public void setId(final int id) {
	this.id.set(id);
    }

    public IntegerProperty idProperty() {
	return id;
    }
    
    public int getPatientID(){
        return patientID.get();
    }
    
    public void setPatientID(final int patientID){
        this.patientID.set(patientID);
    }
    
    public IntegerProperty patientIDProperty(){
        return patientID;
    }
    
    public Timestamp getDateOfMed() {
        return Timestamp.valueOf(dateOfMed.get().atStartOfDay());
    }
    
    public void setDateOfMed(final Timestamp dateOfMed) {
        this.dateOfMed.set(dateOfMed.toLocalDateTime().toLocalDate());
    }
    
    public ObjectProperty<LocalDate> dateOfMedProperty(){
        return dateOfMed;
    }
    
    public LocalDate getLocalDateDateOfMed(){
        return dateOfMed.get();
    }
    
    public void setLocalDateDateOfMed(LocalDate dateOfMed){
        this.dateOfMed.set(dateOfMed);
    }
    
    public String getMed() {
	return med.get();
    }

    public void setMed(final String med) {
	this.med.set(med);
    }

    public StringProperty medProperty() {
	return med;
    }
    
    public double getUnitCost() {
	return unitCost.get();
    }

    public void setUnitCost(final double unitCost) {
	this.unitCost.set(unitCost);
    }

    public DoubleProperty unitCostProperty() {
	return unitCost;
    }

    public double getUnits() {
	return units.get();
    }

    public void setUnits(final double units) {
	this.units.set(units);
    }

    public DoubleProperty unitsProperty() {
	return units;
    }
    
    @Override
    public String toString() {
        return "Medication" + "\nid= " + id 
                + "\npatientID= " + patientID 
                + "\ndateOfMed= " + dateOfMed 
                + "\nmed= " + med 
                + "\nunitCost= " + unitCost 
                + "\nunits= " + units;
    }    

}
