/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.vrud.hospitalnavigation.entities;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author v_rudma
 */
public class Patient {

    private final IntegerProperty patientID;
    private final StringProperty lastName;
    private final StringProperty firstName;
    private final StringProperty diagnosis;
    private final ObjectProperty<LocalDate> admissionDate;
    private final ObjectProperty<LocalDate> releaseDate;
    private final List<Inpatient> listOfInpatients;
    private final List<Medication> listOfMedications;
    private final List<Surgical> listOfSurgicals;

    public Patient() {

        super();
        this.patientID = new SimpleIntegerProperty(-1);
        this.lastName = new SimpleStringProperty("");
        this.firstName = new SimpleStringProperty("");
        this.diagnosis = new SimpleStringProperty("");
        this.admissionDate = new SimpleObjectProperty<>(LocalDate.now());
        this.releaseDate = new SimpleObjectProperty<>(LocalDate.now());
        this.listOfInpatients = new ArrayList<>();
        this.listOfMedications = new ArrayList<>();
        this.listOfSurgicals = new ArrayList<>();

    }

    public Patient(int patientID, String lastName, String firstName, String diagnosis, Timestamp admissionDate, Timestamp releaseDate) {

        super();
        this.patientID = new SimpleIntegerProperty(patientID);
        this.lastName = new SimpleStringProperty(lastName);
        this.firstName = new SimpleStringProperty(firstName);
        this.diagnosis = new SimpleStringProperty(diagnosis);
        this.admissionDate = new SimpleObjectProperty<>(admissionDate.toLocalDateTime().toLocalDate());
        this.releaseDate = new SimpleObjectProperty<>(releaseDate.toLocalDateTime().toLocalDate());
        this.listOfInpatients = new ArrayList<>();
        this.listOfMedications = new ArrayList<>();
        this.listOfSurgicals = new ArrayList<>();

    }

    public List<Inpatient> getListOfInpatients() {
        return listOfInpatients;
    }

    public List<Medication> getListOfMedications() {
        return listOfMedications;
    }

    public List<Surgical> getListOfSurgicals() {
        return listOfSurgicals;
    }

    public int getPatientID() {
        return patientID.get();
    }

    public void setPatientID(final int patientID) {
        this.patientID.set(patientID);
    }

    public IntegerProperty patientIDProperty() {
        return patientID;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(final String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(final String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public String getDiagnosis() {
        return diagnosis.get();
    }

    public void setDiagnosis(final String diagnosis) {
        this.diagnosis.set(diagnosis);
    }

    public StringProperty diagnosisProperty() {
        return diagnosis;
    }
    
    public Timestamp getAdmissionDate() {
        return Timestamp.valueOf(admissionDate.get().atStartOfDay());
    }
    
    public void setAdmissionDate(final Timestamp admissionDate) {
        this.admissionDate.set(admissionDate.toLocalDateTime().toLocalDate());
    }
    
    public ObjectProperty<LocalDate> admissionDateProperty(){
        return admissionDate;
    }
    
    public LocalDate getLocalDateAdmissionDate() {
        return admissionDate.get();
    }
    
    public void setLocalDateAdmissionDate(LocalDate AdmissionDate){
        this.admissionDate.set(AdmissionDate);
    }
    
    public Timestamp getReleaseDate() {
        return Timestamp.valueOf(releaseDate.get().atStartOfDay());
    }
    
    public void setReleaseDate(final Timestamp releaseDate) {
        this.releaseDate.set(releaseDate.toLocalDateTime().toLocalDate());
    }
    
    public ObjectProperty<LocalDate> releaseDateProperty(){
        return releaseDate;
    }    

    @Override
    public String toString() {
        return "Patient" + "\npatientID= " + patientID
                + "\nlastName= " + lastName
                + "\nfirstName= " + firstName
                + "\ndiagnosis= " + diagnosis
                + "\nadmissionDate= " + admissionDate
                + "\nreleaseDate= " + releaseDate;
    }

}
