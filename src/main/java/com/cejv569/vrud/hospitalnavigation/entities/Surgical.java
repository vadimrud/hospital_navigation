/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.vrud.hospitalnavigation.entities;


import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author v_rudma
 */
public class Surgical {
    
    private final IntegerProperty id;
    private final IntegerProperty patientID;
    private final ObjectProperty<LocalDate> dateOfSurgery;
    private final StringProperty surgery;
    private final DoubleProperty roomFee;
    private final DoubleProperty surgeonFee;
    private final DoubleProperty supplies;
    
    public Surgical(){
        
        super();
        this.id = new SimpleIntegerProperty(-1);
        this.patientID = new SimpleIntegerProperty(-1);
        this.dateOfSurgery = new SimpleObjectProperty<>(LocalDate.now());
        this.surgery = new SimpleStringProperty("");
        this.roomFee = new SimpleDoubleProperty(0.0);
        this.surgeonFee = new SimpleDoubleProperty(0.0);
        this.supplies = new SimpleDoubleProperty(0.0);
         
    }
    
    public Surgical(int id, int patientID, Timestamp dateOfSurgery, String surgery, double roomFee, double surgeonFee, double supplies){
        
        super();
        this.id = new SimpleIntegerProperty(id);
        this.patientID = new SimpleIntegerProperty(patientID);
        this.dateOfSurgery = new SimpleObjectProperty<>(dateOfSurgery.toLocalDateTime().toLocalDate());
        this.surgery = new SimpleStringProperty(surgery);
        this.roomFee = new SimpleDoubleProperty(roomFee);
        this.surgeonFee = new SimpleDoubleProperty(surgeonFee);
        this.supplies = new SimpleDoubleProperty(supplies);
         
    }    
    
    public int getId() {
	return id.get();
    }

    public void setId(final int id) {
	this.id.set(id);
    }

    public IntegerProperty idProperty() {
	return id;
    }
    
    public int getPatientID(){
        return patientID.get();
    }
    
    public void setPatientID(final int patientID){
        this.patientID.set(patientID);
    }
    
    public IntegerProperty patientIDProperty(){
        return patientID;
    }
    
    public Timestamp getDateOfSurgery() {
        return Timestamp.valueOf(dateOfSurgery.get().atStartOfDay());
    }
    
    public void setDateOfSurgery(final Timestamp dateOfSurgery) {
        this.dateOfSurgery.set(dateOfSurgery.toLocalDateTime().toLocalDate());
    }
    
    public ObjectProperty<LocalDate> dateOfSurgeryProperty(){
        return dateOfSurgery;
    }
    
    public LocalDate getLocalDateDateOfSurgery(){
        return dateOfSurgery.get();
    }
    
    public void setLocalDateDateOfSurgery(LocalDate dateOfSurgery){
        this.dateOfSurgery.set(dateOfSurgery);
    }
    
    public String getSurgery() {
	return surgery.get();
    }

    public void setSurgery(final String surgery) {
	this.surgery.set(surgery);
    }

    public StringProperty surgeryProperty() {
	return surgery;
    }
    
    public double getRoomFee() {
	return roomFee.get();
    }

    public void setRoomFee(final double roomFee) {
	this.roomFee.set(roomFee);
    }

    public DoubleProperty roomFeeProperty() {
	return roomFee;
    }

    public double getSurgeonFee() {
	return surgeonFee.get();
    }

    public void setSurgeonFee(final double surgeonFee) {
	this.surgeonFee.set(surgeonFee);
    }

    public DoubleProperty surgeonFeeProperty() {
	return surgeonFee;
    }    
    
    public double getSupplies() {
	return supplies.get();
    }

    public void setSupplies(final double supplies) {
	this.supplies.set(supplies);
    }

    public DoubleProperty suppliesProperty() {
	return supplies;
    }
    
    @Override
    public String toString() {
        return "Surgical" + "\nid= " + id 
                + "\npatientID= " + patientID 
                + "\ndateOfSurgery= " + dateOfSurgery
                + "\nsurgery= " + surgery 
                + "\nroomFee= " + roomFee 
                + "\nsurgeonFee= " + surgeonFee
                + "\nsupplies= " + supplies;
    } 
    
    
}
