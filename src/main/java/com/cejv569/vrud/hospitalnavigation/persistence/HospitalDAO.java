/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.vrud.hospitalnavigation.persistence;

import com.cejv569.vrud.hospitalnavigation.entities.Inpatient;
import com.cejv569.vrud.hospitalnavigation.entities.Medication;
import com.cejv569.vrud.hospitalnavigation.entities.Patient;
import com.cejv569.vrud.hospitalnavigation.entities.Surgical;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author v_rudma
 */
public interface HospitalDAO {
    
    //read stuff
    public List<Patient> findAll() throws SQLException;
    public Patient findID(int patientID) throws SQLException;
    public List<Patient> findLastName(String lastName) throws SQLException; 

    //Create stuff
    public int create(Patient patient) throws SQLException;
    public int create(Inpatient inpatient) throws SQLException;
    public int create(Medication medication) throws SQLException;
    public int create(Surgical surgical) throws SQLException;   
    
    //update stuff
    public int update(Patient patient) throws SQLException;
    public int update(Inpatient inpatient) throws SQLException;
    public int update(Medication medication) throws SQLException;
    public int update(Surgical surgical) throws SQLException;
    
    //delete stuff
    public int deletePatient(int patientID) throws SQLException;
    public int deleteInpatient(int patientID, int id) throws SQLException;
    public int deleteMedication(int patientID, int id) throws SQLException;
    public int deleteSurgical(int patientID, int id) throws SQLException;
    
    //next
    public Patient findNextByID(Patient patient) throws SQLException;
    public Inpatient findNextByID(Inpatient inpatient) throws SQLException;
    public Medication findNextByID(Medication medication) throws SQLException;
    public Surgical findNextByID(Surgical surgical) throws SQLException;
    
    //prev
    public Patient findPrevByID(Patient patient) throws SQLException;
    public Inpatient findPrevByID(Inpatient inpatient) throws SQLException;
    public Medication findPrevByID(Medication medication) throws SQLException;
    public Surgical findPrevByID(Surgical surgical) throws SQLException;
    
    //save
    public int savePatient(Patient patient) throws SQLException;
    public int saveInpatient(Inpatient inpatient) throws SQLException;
    public int saveMedication(Medication medication) throws SQLException;
    public int saveSurgical(Surgical surgical) throws SQLException;
    
    //find children
    public Inpatient findInpatient(int id) throws SQLException;
    public Medication findMedication(int id) throws SQLException;
    public Surgical findSurgical(int id) throws SQLException;
    
}
