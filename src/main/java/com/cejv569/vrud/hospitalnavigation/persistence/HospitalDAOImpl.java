/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.vrud.hospitalnavigation.persistence;

import com.cejv569.vrud.hospitalnavigation.entities.Inpatient;
import com.cejv569.vrud.hospitalnavigation.entities.Medication;
import com.cejv569.vrud.hospitalnavigation.entities.Patient;
import com.cejv569.vrud.hospitalnavigation.entities.Surgical;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author v_rudma
 */
public class HospitalDAOImpl implements HospitalDAO {

//    private final String url = "jdbc:mysql://localhost:1507/hospitaldb";
//    private final String user = "root";
//    private final String password = "pancake";
    
    private final String url = "jdbc:mysql://localhost:3306/hospitaldb";
    private final String user = "TheUser";
    private final String password = "pancake";

    public HospitalDAOImpl() {
        super();
    }

    /**
     * this method finds and displays all records
     *
     * @return List of all patients
     * @throws java.sql.SQLException
     */
    @Override
    public List<Patient> findAll() throws SQLException {
        List<Patient> rows = new ArrayList<>();

        String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                Patient patient = new Patient();
                patient.setPatientID(resultSet.getInt("PATIENTID"));
                patient.setLastName(resultSet.getString("LASTNAME"));
                patient.setFirstName(resultSet.getString("FIRSTNAME"));
                patient.setDiagnosis(resultSet.getString("DIAGNOSIS"));
                patient.setAdmissionDate(resultSet.getTimestamp("ADMISSIONDATE"));
                patient.setReleaseDate(resultSet.getTimestamp("RELEASEDATE"));
                findAllInpatients(patient);
                findAllMedications(patient);
                findAllSurgicals(patient);
                rows.add(patient);
            }
        }

        return rows;
    }

    /**
     * this method is called to populate and return the list of inpatient
     *
     * @param patient
     * @throws SQLException
     */
    private void findAllInpatients(Patient patient) throws SQLException {
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES FROM INPATIENT WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, patient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    Inpatient inpatient = new Inpatient();

                    inpatient.setPatientID(resultSet.getInt("PATIENTID"));
                    inpatient.setDateOfStay(resultSet.getTimestamp("DATEOFSTAY"));
                    inpatient.setRoomNumber(resultSet.getString("ROOMNUMBER"));
                    inpatient.setDailyRate(resultSet.getDouble("DAILYRATE"));
                    inpatient.setSupplies(resultSet.getDouble("SUPPLIES"));
                    inpatient.setServices(resultSet.getDouble("SERVICES"));
                    inpatient.setId(resultSet.getInt("ID"));
                    patient.getListOfInpatients().add(inpatient);
                }
            }
        }
    }

    /**
     * this method is called to populate and return the list of medication
     *
     * @param patient
     * @throws SQLException
     */
    private void findAllMedications(Patient patient) throws SQLException {
        String selectQuery = "SELECT ID, PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, patient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    Medication medication = new Medication();

                    medication.setPatientID(resultSet.getInt("PATIENTID"));
                    medication.setDateOfMed(resultSet.getTimestamp("DATEOFMED"));
                    medication.setMed(resultSet.getString("MED"));
                    medication.setUnitCost(resultSet.getDouble("UNITCOST"));
                    medication.setUnits(resultSet.getDouble("UNITS"));
                    medication.setId(resultSet.getInt("ID"));
                    patient.getListOfMedications().add(medication);
                }
            }
        }
    }

    /**
     * this method is called to populate and return the list of surgicals
     *
     * @param patient
     * @throws SQLException
     */
    private void findAllSurgicals(Patient patient) throws SQLException {
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, patient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    Surgical surgical = new Surgical();

                    surgical.setPatientID(resultSet.getInt("PATIENTID"));
                    surgical.setDateOfSurgery(resultSet.getTimestamp("DATEOFSURGERY"));
                    surgical.setSurgery(resultSet.getString("SURGERY"));
                    surgical.setRoomFee(resultSet.getDouble("ROOMFEE"));
                    surgical.setSurgeonFee(resultSet.getDouble("SURGEONFEE"));
                    surgical.setSupplies(resultSet.getDouble("SUPPLIES"));
                    surgical.setId(resultSet.getInt("ID"));
                    patient.getListOfSurgicals().add(surgical);
                }
            }
        }
    }

    /**
     * this method is called to return the patient according to an ID search
     *
     * @param patientID
     * @return
     * @throws SQLException
     */
    @Override
    public Patient findID(int patientID) throws SQLException {
        Patient patient = new Patient();
        String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, patientID);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {

                    patient.setPatientID(resultSet.getInt("PATIENTID"));
                    patient.setLastName(resultSet.getString("LASTNAME"));
                    patient.setFirstName(resultSet.getString("FIRSTNAME"));
                    patient.setDiagnosis(resultSet.getString("DIAGNOSIS"));
                    patient.setAdmissionDate(resultSet.getTimestamp("ADMISSIONDATE"));
                    patient.setReleaseDate(resultSet.getTimestamp("RELEASEDATE"));
                    findAllInpatients(patient);
                    findAllMedications(patient);
                    findAllSurgicals(patient);

                }
            }
        }
        return patient;
    }
    
    /**
     * method is called to find next patient entry
     * @param patient
     * @return
     * @throws SQLException 
     */
    @Override
    public Patient findNextByID(Patient patient) throws SQLException{
        String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MIN(PATIENTID) from PATIENT WHERE PATIENTID > ?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, patient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    patient.setPatientID(resultSet.getInt("PATIENTID"));
                    patient.setLastName(resultSet.getString("LASTNAME"));
                    patient.setFirstName(resultSet.getString("FIRSTNAME"));
                    patient.setDiagnosis(resultSet.getString("DIAGNOSIS"));
                    patient.setAdmissionDate(resultSet.getTimestamp("ADMISSIONDATE"));
                    patient.setReleaseDate(resultSet.getTimestamp("RELEASEDATE"));
                    findAllInpatients(patient);
                    findAllMedications(patient);
                    findAllSurgicals(patient);
                }
            }
        }
        return patient;
    }
    
    /**
     * This method is called to find next patient by ID
     * @param patient
     * @return
     * @throws SQLException 
     */
    @Override
    public Patient findPrevByID(Patient patient) throws SQLException{
        String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE PATIENTID = (SELECT MAX(PATIENTID) from PATIENT WHERE PATIENTID < ?)";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, patient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    patient.setPatientID(resultSet.getInt("PATIENTID"));
                    patient.setLastName(resultSet.getString("LASTNAME"));
                    patient.setFirstName(resultSet.getString("FIRSTNAME"));
                    patient.setDiagnosis(resultSet.getString("DIAGNOSIS"));
                    patient.setAdmissionDate(resultSet.getTimestamp("ADMISSIONDATE"));
                    patient.setReleaseDate(resultSet.getTimestamp("RELEASEDATE"));
                    findAllInpatients(patient);
                    findAllMedications(patient);
                    findAllSurgicals(patient);
                }
            }
        }
        return patient;
    }

    /**
     * this method is called to find all patient records with given last name
     *
     * @param lastName
     * @return List of patients matching param
     * @throws SQLException
     */
    @Override
    public List<Patient> findLastName(String lastName) throws SQLException {
        List<Patient> rows = new ArrayList<>();

        String selectQuery = "SELECT PATIENTID, LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE FROM PATIENT WHERE LASTNAME = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setString(1, lastName);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    Patient patient = new Patient();
                    patient.setPatientID(resultSet.getInt("PATIENTID"));
                    patient.setLastName(resultSet.getString("LASTNAME"));
                    patient.setFirstName(resultSet.getString("FIRSTNAME"));
                    patient.setDiagnosis(resultSet.getString("DIAGNOSIS"));
                    patient.setAdmissionDate(resultSet.getTimestamp("ADMISSIONDATE"));
                    patient.setReleaseDate(resultSet.getTimestamp("RELEASEDATE"));
                    findAllInpatients(patient);
                    findAllMedications(patient);
                    findAllSurgicals(patient);
                    rows.add(patient);
                }
            }
        }
        return rows;
    }

    /**
     * this method is called to create a new entry into patient table
     *
     * @param patient
     * @return
     * @throws SQLException
     */
    @Override
    public int create(Patient patient) throws SQLException {
        int records;

        String sql = "INSERT INTO PATIENT (LASTNAME, FIRSTNAME, DIAGNOSIS, ADMISSIONDATE, RELEASEDATE) values (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setString(1, patient.getLastName());
            pStatement.setString(2, patient.getFirstName());
            pStatement.setString(3, patient.getDiagnosis());
            pStatement.setTimestamp(4, patient.getAdmissionDate());
            pStatement.setTimestamp(5, patient.getReleaseDate());
            records = pStatement.executeUpdate();
            
            ResultSet rs = pStatement.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
                recordNum = rs.getInt(1);
            }
            patient.setPatientID(recordNum);
            
        }
        
        return records;
    }

    /**
     * this method is called to create a new entry into inpatient table
     *
     * @param inpatient
     * @return
     * @throws SQLException
     */
    @Override
    public int create(Inpatient inpatient) throws SQLException {
        int records;

        String sql = "INSERT INTO INPATIENT (DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES) values (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setTimestamp(1, inpatient.getDateOfStay());
            pStatement.setString(2, inpatient.getRoomNumber());
            pStatement.setDouble(3, inpatient.getDailyRate());
            pStatement.setDouble(4, inpatient.getSupplies());
            pStatement.setDouble(5, inpatient.getServices());
            records = pStatement.executeUpdate();
            
            ResultSet rs = pStatement.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
                recordNum = rs.getInt(1);
            }
            inpatient.setId(recordNum);
            
        }
        return records;
    }
    
    /**
     * this method is called to create a new entry into medication table
     * @param medication
     * @return
     * @throws SQLException 
     */
    @Override
    public int create(Medication medication) throws SQLException {
        int records;

        String sql = "INSERT INTO MEDICATION (DATEOFMED, MED, UNITCOST, UNITS) values (?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setTimestamp(1, medication.getDateOfMed());
            pStatement.setString(2, medication.getMed());
            pStatement.setDouble(3, medication.getUnitCost());
            pStatement.setDouble(4, medication.getUnits());
            records = pStatement.executeUpdate();
            
            ResultSet rs = pStatement.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
                recordNum = rs.getInt(1);
            }
            medication.setId(recordNum);
            
        }
        return records;
    }
    
    /**
     * this method is called to create a new entry into surgical table
     * @param surgical
     * @return
     * @throws SQLException 
     */
    @Override
    public int create(Surgical surgical) throws SQLException{
        int records;
        
        String sql = "INSERT INTO SURGICAL (DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES) values (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setTimestamp(1, surgical.getDateOfSurgery());
            pStatement.setString(2, surgical.getSurgery());
            pStatement.setDouble(3, surgical.getRoomFee());
            pStatement.setDouble(4, surgical.getSurgeonFee());
            pStatement.setDouble(5, surgical.getSupplies());
            records = pStatement.executeUpdate();
            
            ResultSet rs = pStatement.getGeneratedKeys();
            int recordNum = -1;
            if (rs.next()) {
                recordNum = rs.getInt(1);
            }
            surgical.setId(recordNum);
            
        }
        return records;
    }
    
    /**
     * This method is called to update a record in the Patient table
     * @param patient
     * @return
     * @throws SQLException 
     */
    @Override
    public int update(Patient patient) throws SQLException{
        int records;
        String query = "UPDATE PATIENT SET LASTNAME=?, FIRSTNAME=?, DIAGNOSIS=?, ADMISSIONDATE=?, RELEASEDATE=? WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setString(1, patient.getLastName());
            pStatement.setString(2, patient.getFirstName());
            pStatement.setString(3, patient.getDiagnosis());
            pStatement.setTimestamp(4, patient.getAdmissionDate());
            pStatement.setTimestamp(5, patient.getReleaseDate());
            pStatement.setInt(6, patient.getPatientID());
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * This method is called to update a record in the Inpatient table
     * @param inpatient
     * @return
     * @throws SQLException 
     */
    @Override
    public int update(Inpatient inpatient) throws SQLException{
        int records;
        String query = "UPDATE INPATIENT SET DATEOFSTAY=?, ROOMNUMBER=?, DAILYRATE=?, SUPPLIES=?, SERVICES=? WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, inpatient.getDateOfStay());
            pStatement.setString(2, inpatient.getRoomNumber());
            pStatement.setDouble(3, inpatient.getDailyRate());
            pStatement.setDouble(4, inpatient.getSupplies());            
            pStatement.setDouble(5, inpatient.getServices());
            pStatement.setInt(6, inpatient.getId());
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * This method is called to update a record in the Medication table
     * @param medication
     * @return
     * @throws SQLException 
     */
    @Override
    public int update(Medication medication) throws SQLException{
        int records;
        String query = "UPDATE MEDICATION SET DATEOFMED=?, MED=?, UNITCOST=?, UNITS=? WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, medication.getDateOfMed());
            pStatement.setString(2, medication.getMed());
            pStatement.setDouble(3, medication.getUnitCost());
            pStatement.setDouble(4, medication.getUnits());
            pStatement.setInt(5, medication.getId());
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * This method is called to update a record in the Surgical table
     * @param surgical
     * @return
     * @throws SQLException 
     */
    @Override
    public int update(Surgical surgical) throws SQLException{
        int records;
        String query = "UPDATE SURGICAL SET DATEOFSURGERY=?, SURGERY=?, ROOMFEE=?, SURGEONFEE=?, SUPPLIES=? WHERE ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setTimestamp(1, surgical.getDateOfSurgery());
            pStatement.setString(2, surgical.getSurgery());
            pStatement.setDouble(3, surgical.getRoomFee());
            pStatement.setDouble(4, surgical.getSurgeonFee());            
            pStatement.setDouble(5, surgical.getSupplies());
            pStatement.setInt(6, surgical.getId());
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * this method is called to delete one record from Inpatient table
     * @param patientID
     * @param id
     * @return
     * @throws SQLException 
     */
    @Override
    public int deleteInpatient(int patientID, int id) throws SQLException{
        int records;
        String query = "DELETE FROM INPATIENT WHERE PATIENTID = ? AND ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patientID);
            pStatement.setInt(2, id);
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * this method is called to delete one record from Medication table
     * @param patientID
     * @param id
     * @return
     * @throws SQLException 
     */
    @Override
    public int deleteMedication(int patientID, int id) throws SQLException{
        int records;
        String query = "DELETE FROM MEDICATION WHERE PATIENTID = ? AND ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patientID);
            pStatement.setInt(2, id);
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * this method is called to delete one record from Surgical table
     * @param patientID
     * @param id
     * @return
     * @throws SQLException 
     */
    @Override
    public int deleteSurgical(int patientID, int id) throws SQLException{
        int records;
        String query = "DELETE FROM SURGICAL WHERE PATIENTID = ? AND ID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patientID);
            pStatement.setInt(2, id);
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * This method is called to delete all entries in Inpatient table 
     * @param patientID
     * @return
     * @throws SQLException 
     */
    public int deleteAllInpatient(int patientID) throws SQLException{
        int records;
        String query = "DELETE FROM INPATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patientID);
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * This method is called to delete all entries in Medication table 
     * @param patientID
     * @return
     * @throws SQLException 
     */
    public int deleteAllMedication(int patientID) throws SQLException{
        int records;
        String query = "DELETE FROM MEDICATION WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patientID);
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * This method is called to delete all entries in Surgical table 
     * @param patientID
     * @return
     * @throws SQLException 
     */
    public int deleteAllSurgical(int patientID) throws SQLException{
        int records;
        String query = "DELETE FROM SURGICAL WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patientID);
            records = pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * This method is called to delete an entry in Patient table
     * @param patientID
     * @return
     * @throws SQLException 
     */
    @Override
    public int deletePatient(int patientID) throws SQLException{
        int records;
        records = deleteAllInpatient(patientID);
        records += deleteAllMedication(patientID);
        records += deleteAllSurgical(patientID);
        String query = "DELETE FROM PATIENT WHERE PATIENTID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);) {
            pStatement.setInt(1, patientID);
            records += pStatement.executeUpdate();
        }
        return records;
    }
    
    /**
     * This method is called to find the next inpatient by ID
     * @param inpatient
     * @return
     * @throws SQLException 
     */
    @Override
    public Inpatient findPrevByID(Inpatient inpatient) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES FROM INPATIENT WHERE ID = (SELECT MAX(ID) FROM INPATIENT WHERE ID < ?) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, inpatient.getId());
            pStatement.setInt(2, inpatient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    inpatient.setPatientID(resultSet.getInt("PATIENTID"));
                    inpatient.setDateOfStay(resultSet.getTimestamp("DATEOFSTAY"));
                    inpatient.setRoomNumber(resultSet.getString("ROOMNUMBER"));
                    inpatient.setDailyRate(resultSet.getDouble("DAILYRATE"));
                    inpatient.setSupplies(resultSet.getDouble("SUPPLIES"));
                    inpatient.setServices(resultSet.getDouble("SERVICES"));
                    inpatient.setId(resultSet.getInt("ID"));
            }
        }
        return inpatient;
    }
    
    /**
     * This method is called to find the prev inpatient by ID
     * @param inpatient
     * @return
     * @throws SQLException 
     */
    @Override
    public Inpatient findNextByID(Inpatient inpatient) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES FROM INPATIENT WHERE ID = (SELECT MIN(ID) FROM INPATIENT WHERE ID > ?) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, inpatient.getId());
            pStatement.setInt(2, inpatient.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    inpatient.setPatientID(resultSet.getInt("PATIENTID"));
                    inpatient.setDateOfStay(resultSet.getTimestamp("DATEOFSTAY"));
                    inpatient.setRoomNumber(resultSet.getString("ROOMNUMBER"));
                    inpatient.setDailyRate(resultSet.getDouble("DAILYRATE"));
                    inpatient.setSupplies(resultSet.getDouble("SUPPLIES"));
                    inpatient.setServices(resultSet.getDouble("SERVICES"));
                    inpatient.setId(resultSet.getInt("ID"));
            }
        }
        return inpatient;
    }
    
    /**
     * This method is called to find the next Medication by ID
     * @param medication
     * @return
     * @throws SQLException 
     */
    @Override
    public Medication findPrevByID(Medication medication) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION WHERE ID = (SELECT MAX(ID) FROM SURGICAL WHERE ID < ?) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, medication.getId());
            pStatement.setInt(2, medication.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    medication.setPatientID(resultSet.getInt("PATIENTID"));
                    medication.setDateOfMed(resultSet.getTimestamp("DATEOFMED"));
                    medication.setMed(resultSet.getString("MED"));
                    medication.setUnitCost(resultSet.getDouble("UNITCOST"));
                    medication.setUnits(resultSet.getDouble("UNITS"));
                    medication.setId(resultSet.getInt("ID"));
            }
        }
        return medication;
    }
    
    /**
     * This method is called to find the previous Medication by ID
     * @param medication
     * @return
     * @throws SQLException 
     */
    @Override
    public Medication findNextByID(Medication medication) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION WHERE ID = (SELECT MIN(ID) FROM SURGICAL WHERE ID > ?) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, medication.getId());
            pStatement.setInt(2, medication.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    medication.setPatientID(resultSet.getInt("PATIENTID"));
                    medication.setDateOfMed(resultSet.getTimestamp("DATEOFMED"));
                    medication.setMed(resultSet.getString("MED"));
                    medication.setUnitCost(resultSet.getDouble("UNITCOST"));
                    medication.setUnits(resultSet.getDouble("UNITS"));
                    medication.setId(resultSet.getInt("ID"));
            }
        }
        return medication;
    }
    
    /**
     * This method is called to find the next surgical by ID
     * @param surgical
     * @return
     * @throws SQLException 
     */
    @Override
    public Surgical findNextByID(Surgical surgical) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = (SELECT MIN(ID) FROM SURGICAL WHERE ID > ?) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, surgical.getId());
            pStatement.setInt(2, surgical.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    surgical.setPatientID(resultSet.getInt("PATIENTID"));
                    surgical.setDateOfSurgery(resultSet.getTimestamp("DATEOFSURGERY"));
                    surgical.setSurgery(resultSet.getString("SURGERY"));
                    surgical.setRoomFee(resultSet.getDouble("ROOMFEE"));
                    surgical.setSurgeonFee(resultSet.getDouble("SURGEONFEE"));
                    surgical.setSupplies(resultSet.getDouble("SUPPLIES"));
                    surgical.setId(resultSet.getInt("ID"));
            }
        }
        return surgical;
    }
    
    /**
     * This method is called to find the next surgical by ID
     * @param surgical
     * @return
     * @throws SQLException 
     */
    @Override
    public Surgical findPrevByID(Surgical surgical) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = (SELECT MAX(ID) FROM SURGICAL WHERE ID < ?) AND PATIENTID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, surgical.getId());
            pStatement.setInt(2, surgical.getPatientID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    surgical.setPatientID(resultSet.getInt("PATIENTID"));
                    surgical.setDateOfSurgery(resultSet.getTimestamp("DATEOFSURGERY"));
                    surgical.setSurgery(resultSet.getString("SURGERY"));
                    surgical.setRoomFee(resultSet.getDouble("ROOMFEE"));
                    surgical.setSurgeonFee(resultSet.getDouble("SURGEONFEE"));
                    surgical.setSupplies(resultSet.getDouble("SUPPLIES"));
                    surgical.setId(resultSet.getInt("ID"));
            }
        }
        return surgical;
    }
    
    /**
     * This method is called to either create or update
     * @param patient
     * @return
     * @throws SQLException 
     */
    @Override
    public int savePatient(Patient patient) throws SQLException{
        int records;
        if (patient.getPatientID() == -1) {
            records = create(patient);
        } else {
            records = update(patient);
        }
        return records;
    }
    
    /**
     * This method is called to either create or update
     * @param inpatient
     * @return
     * @throws SQLException 
     */
    @Override
    public int saveInpatient(Inpatient inpatient) throws SQLException{
        int records;
        if (inpatient.getId() == -1) {
            records = create(inpatient);
        } else {
            records = update(inpatient);
        }
        return records;
    }
    
    /**
     * This method is called to either create or update
     * @param medication
     * @return
     * @throws SQLException 
     */
    @Override
    public int saveMedication(Medication medication) throws SQLException{
        int records;
        if (medication.getId() == -1) {
            records = create(medication);
        } else {
            records = update(medication);
        }
        return records;
    } 
    
    /**
     * This method is called to either create or update
     * @param surgical
     * @return
     * @throws SQLException 
     */
    @Override
    public int saveSurgical(Surgical surgical) throws SQLException{
        int records;
        if (surgical.getId() == -1) {
            records = create(surgical);
        } else {
            records = update(surgical);
        }
        return records;
    }
    
    /**
     * Find inpatient by ID
     * @param id
     * @return
     * @throws SQLException 
     */
    @Override
    public Inpatient findInpatient(int id) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSTAY, ROOMNUMBER, DAILYRATE, SUPPLIES, SERVICES FROM INPATIENT WHERE ID = ?";
        
        Inpatient inpatient = new Inpatient();
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    
                    inpatient.setPatientID(resultSet.getInt("PATIENTID"));
                    inpatient.setDateOfStay(resultSet.getTimestamp("DATEOFSTAY"));
                    inpatient.setRoomNumber(resultSet.getString("ROOMNUMBER"));
                    inpatient.setDailyRate(resultSet.getDouble("DAILYRATE"));
                    inpatient.setSupplies(resultSet.getDouble("SUPPLIES"));
                    inpatient.setServices(resultSet.getDouble("SERVICES"));
                    inpatient.setId(resultSet.getInt("ID"));
            }
        }
        return inpatient;        
    }
    
    /**
     * finds medication by ID
     * @param id
     * @return
     * @throws SQLException 
     */
    @Override
    public Medication findMedication(int id) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID,DATEOFMED,MED,UNITCOST,UNITS FROM MEDICATION WHERE ID = ?";
        Medication medication = new Medication();
        
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    medication.setPatientID(resultSet.getInt("PATIENTID"));
                    medication.setDateOfMed(resultSet.getTimestamp("DATEOFMED"));
                    medication.setMed(resultSet.getString("MED"));
                    medication.setUnitCost(resultSet.getDouble("UNITCOST"));
                    medication.setUnits(resultSet.getDouble("UNITS"));
                    medication.setId(resultSet.getInt("ID"));
            }
        }
        return medication;
    }
    
    /**
     * Find surgical by ID
     * @param id
     * @return
     * @throws SQLException 
     */
    public Surgical findSurgical(int id) throws SQLException{
        String selectQuery = "SELECT ID, PATIENTID, DATEOFSURGERY, SURGERY, ROOMFEE, SURGEONFEE, SUPPLIES FROM SURGICAL WHERE ID = ?";
        Surgical surgical = new Surgical();
        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, id);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                    surgical.setPatientID(resultSet.getInt("PATIENTID"));
                    surgical.setDateOfSurgery(resultSet.getTimestamp("DATEOFSURGERY"));
                    surgical.setSurgery(resultSet.getString("SURGERY"));
                    surgical.setRoomFee(resultSet.getDouble("ROOMFEE"));
                    surgical.setSurgeonFee(resultSet.getDouble("SURGEONFEE"));
                    surgical.setSupplies(resultSet.getDouble("SUPPLIES"));
                    surgical.setId(resultSet.getInt("ID"));
            }
        }
        return surgical;
    }
}
