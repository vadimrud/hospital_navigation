/**
 * Sample Skeleton for 'surgicalFXML.fxml' Controller Class
 */

package com.cejv569.vrud.hospitalnavigation.surgicalController;

import com.cejv569.vrud.hospitalnavigation.entities.Inpatient;
import com.cejv569.vrud.hospitalnavigation.entities.Patient;
import com.cejv569.vrud.hospitalnavigation.entities.Surgical;
import com.cejv569.vrud.hospitalnavigation.persistence.HospitalDAO;
import com.cejv569.vrud.hospitalnavigation.persistence.HospitalDAOImpl;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

public class SurgicalFXMLController {
    
    private HospitalDAO hospitalDAO;
    private Patient patient;
    private Surgical surgical;
    private Alert alertInfo;
    private Alert alertError;
    
    public SurgicalFXMLController(){
        super();
        surgical = new Surgical();
        patient = new Patient();
        hospitalDAO = new HospitalDAOImpl();
        alertInfo = new Alert(Alert.AlertType.INFORMATION);
        alertError = new Alert(Alert.AlertType.ERROR);
    }

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="patientIDTextField"
    private TextField patientIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="surgicalIDTextField"
    private TextField surgicalIDTextField; // Value injected by FXMLLoader

    @FXML // fx:id="surgeryTextField"
    private TextField surgeryTextField; // Value injected by FXMLLoader

    @FXML // fx:id="roomFeeTextField"
    private TextField roomFeeTextField; // Value injected by FXMLLoader

    @FXML // fx:id="surgeonFeeTextField"
    private TextField surgeonFeeTextField; // Value injected by FXMLLoader

    @FXML // fx:id="dateOfSurgeryField"
    private DatePicker dateOfSurgeryField; // Value injected by FXMLLoader

    @FXML // fx:id="prevBtn"
    private Button prevBtn; // Value injected by FXMLLoader

    @FXML // fx:id="clearBtn"
    private Button clearBtn; // Value injected by FXMLLoader

    @FXML // fx:id="saveBtn"
    private Button saveBtn; // Value injected by FXMLLoader

    @FXML // fx:id="deleteBtn"
    private Button deleteBtn; // Value injected by FXMLLoader

    @FXML // fx:id="nextBtn"
    private Button nextBtn; // Value injected by FXMLLoader

    @FXML // fx:id="searchBtn"
    private Button searchBtn; // Value injected by FXMLLoader

    @FXML
    void clearActionEvent(ActionEvent event) {
        
        surgical.setId(-1);
        surgeryTextField.setText("");
        roomFeeTextField.setText("");
        surgeonFeeTextField.setText("");
        
    }

    @FXML
    void deleteActionEvent(ActionEvent event) throws SQLException {
        hospitalDAO.deleteSurgical(surgical.getPatientID(), surgical.getId());
    }

    @FXML
    void nextActionEvent(ActionEvent event) throws SQLException {
        surgical = hospitalDAO.findNextByID(surgical);
    }

    @FXML
    void prevActionEvent(ActionEvent event) throws SQLException {
        surgical = hospitalDAO.findPrevByID(surgical);
    }

    @FXML
    void saveActionEvent(ActionEvent event) throws SQLException {
        
        if(areFieldsOK()){
            hospitalDAO.saveSurgical(surgical);
        }
    }

    @FXML
    void searchActionEvent(ActionEvent event) throws SQLException {
        surgical = hospitalDAO.findSurgical(surgical.getId());
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        
        Bindings.bindBidirectional(surgicalIDTextField.textProperty(), surgical.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(surgeryTextField.textProperty(), surgical.surgeryProperty());
        Bindings.bindBidirectional(roomFeeTextField.textProperty(), surgical.roomFeeProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(surgeonFeeTextField.textProperty(), surgical.surgeonFeeProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(dateOfSurgeryField.valueProperty(), surgical.dateOfSurgeryProperty());
        Bindings.bindBidirectional(patientIDTextField.textProperty(), patient.patientIDProperty(), new NumberStringConverter());
        
        patientIDTextField.setEditable(false);

    }
    
    /**
     * This method will be called in the the mainController to pass the DAO
     * @param hospitalDAO 
     */
    public void setHospitalDAO(HospitalDAO hospitalDAO){
        this.hospitalDAO = hospitalDAO;
    }
    
    /**
     * This method will be called in the mainController to pass the patient
     * @param patient 
     */
    public void setPatient(Patient patient){
        this.patient = patient;
    }
    
    /**
     * this method is called to check if the strings entered are too long and
     * show an error if too long
     * @param input
     * @param type 
     * @return false if too long, true if ok
     */
    public boolean isStringOK(String input, String type){
        boolean isStringOK = true;
        if(input.length() > 255){
            alertError.setTitle("Input error has occured");
            alertError.setHeaderText("The " + type + " field is too long.");
            alertError.setContentText("This field must contain less than 256 characters");
            alertError.showAndWait();
            isStringOK = false;
        }
        return isStringOK;
    }
    
    /**
     * This method is called to check if double type entries are ok
     * @param input
     * @param type
     * @return true if ok, false if negative
     */
    public boolean isDoubleOK(double input, String type){
        boolean isDoubleOK = true;
        if(input < 0){
            alertError.setTitle("Input error has occured");
            alertError.setHeaderText(null);
            alertError.setContentText("The " + type + " field must be greater than 0.");
            alertError.showAndWait();
            isDoubleOK = false;
        }
        return isDoubleOK;
    }
    
    /**
     * this method is called to check if all fields are ok
     * @return true if ok, false if not ok
     */
    public boolean areFieldsOK(){
        boolean areFieldsOK = true;
        if(isStringOK(surgical.getSurgery(), "Surgery") == false
                || (isDoubleOK(surgical.getSurgeonFee(), "Surgeon Fee") == false)
                || (isDoubleOK(surgical.getSupplies(), "Supplies") == false))
        {
            areFieldsOK = false;
        }
        return areFieldsOK;
    }
    
}
