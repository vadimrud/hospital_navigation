/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.vrud.tests;

import com.cejv569.vrud.hospitalnavigation.entities.Inpatient;
import com.cejv569.vrud.hospitalnavigation.entities.Medication;
import com.cejv569.vrud.hospitalnavigation.entities.Patient;
import com.cejv569.vrud.hospitalnavigation.entities.Surgical;
import com.cejv569.vrud.hospitalnavigation.persistence.HospitalDAO;
import com.cejv569.vrud.hospitalnavigation.persistence.HospitalDAOImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author v_rudma
 */
public class TestDatabase {
    
//    private final String url = "jdbc:mysql://localhost:1507/hospitaldb";
//    private final String user = "root";
//    private final String password = "pancake";
    
    private final String url = "jdbc:mysql://localhost:3306/hospitaldb";
    private final String user = "TheUser";
    private final String password = "pancake";
    
    private HospitalDAO hospitalDAO;
    private Patient patient1;
    private Inpatient inPatient1;
    private Medication medication1;
    private Surgical surgical1;
    private List<Patient> patientList1;
    
    
    @Before
    public void initializeHospitalData(){
        hospitalDAO = new HospitalDAOImpl();
        patient1 = new Patient(1, "Wayne","Bruce","Asthma", new Timestamp(2014-1-23-9-0-0-0), new Timestamp(201-1-25-13-0-0-0));
        inPatient1 = new Inpatient(1, 1, new Timestamp(2014-1-23-9-0-0-0), "A1", 250.00, 75.24, 12.95);
        medication1 = new Medication(1, 1, new Timestamp(2014-01-24-11-0-0-0), "Snickers", 1.25, 5.0);
        surgical1 = new Surgical(1, 1, new Timestamp(2014-1-24-11-0-0-0), "Lung Transplant", 2500.12, 4200.00, 934.23);
        patientList1 = new ArrayList<>();
        patientList1.add(patient1);
    }
    
    
    
    /**
     * This tests to see if findAll() returns the amount of patient i know are in the database
     * @throws SQLException 
     */
    @Test
    public void testFindAll() throws SQLException {
        List<Patient> patients = hospitalDAO.findAll();
         
        assertEquals("# of patients", 5, patients.size());
    }
    
    /**
     * This tests if the patient found with the input of ID = 1 is in fact equal to the patient bean at patientID = 1
     * @throws SQLException 
     */
    @Test
    public void testFindID() throws SQLException{
       Patient findPatient = hospitalDAO.findID(2);
       
       assertEquals("Found incorrect patient", patient1, findPatient);
        
    }
    
    /**
     * This tests if the list of patients with the last name search term equals the list given that the search term is Wayne
     * @throws SQLException 
     */
    @Test
    public void testFindLastName() throws SQLException{
        List<Patient> patients = hospitalDAO.findLastName("Wayne");
        
        assertEquals("# of patients with same last name", 1, patients.size());
    }
    
    /**
     * this tests if a patient record is created
     * @throws SQLException 
     */
    @Test
    public void testCreatePatient() throws SQLException{
        Patient testPatient = new Patient(6, "Vadim", "Rudman", "He's too sick", new Timestamp(Instant.now().toEpochMilli()), new Timestamp(Instant.now().toEpochMilli()));
        
        int records = hospitalDAO.create(testPatient);
        assertEquals("A patient record was not created", 1, records);
    }
    
    /**
     * this tests if an inpatient record is created
     * @throws SQLException 
     */
    @Test
    public void testCreateInpatient() throws SQLException{
        Inpatient testInpatient = new Inpatient(4, 1, new Timestamp(Instant.now().toEpochMilli()), "A2", 25, 25, 25);
        
        int records = hospitalDAO.create(testInpatient);
        assertEquals("An inpatient record was not created", 1, records);
    }
    
    /**
     * this tests if a medication record is created
     * @throws SQLException 
     */
    @Test
    public void testCreateMedication() throws SQLException{
        Medication medication = new Medication(2, 1, new Timestamp(Instant.now().toEpochMilli()), "Oh henry", 25, 25);
        
        int records = hospitalDAO.create(medication);
        assertEquals("A medication record was not created", 1, records);
    }
    
    /**
     * this tests if a surgical record is created
     * @throws SQLException 
     */
    @Test
    public void testCreateSurgical() throws SQLException{
        Surgical testSurgical = new Surgical(2, 1, new Timestamp(Instant.now().toEpochMilli()), "Bat transplant", 25, 25, 25);
        
        int records = hospitalDAO.create(testSurgical);
        assertEquals("An inpatient record was not created", 1, records);
    }
    
    /**
     * this tests if the correct number of deletes happened in the Inpatient table
     * @throws SQLException 
     */
    @Test
    public void ztestDeleteInpatient() throws SQLException{
        int records = hospitalDAO.deleteInpatient(1, 1);
        Patient patientTest = hospitalDAO.findID(1);
        int numOfInpatients = patientTest.getListOfInpatients().size();
        
        assertEquals("An inpatient record was not deleted", 2, numOfInpatients);
    }
    
    /**
     * this tests if the correct number of deletes happened in the Medication table
     * @throws SQLException 
     */
    @Test
    public void ztestDeleteMedication() throws SQLException{
        int records = hospitalDAO.deleteMedication(1, 1);
        Patient patientTest = hospitalDAO.findID(1);
        int numOfMedication = patientTest.getListOfMedications().size();
        
        assertEquals("A medication record was not deleted", 0, numOfMedication);
    }
    
    /**
     * this tests if the correct number of deletes happened in the Surgical table
     * @throws SQLException 
     */
    @Test
    public void ztestDeleteSurgical() throws SQLException{
        int records = hospitalDAO.deleteSurgical(1, 1);
        Patient patientTest = hospitalDAO.findID(1);
        int numOfSurgical = patientTest.getListOfSurgicals().size();
        
        assertEquals("A surgical record was not deleted", 0, numOfSurgical);
    }
    
    /**
     * this tests if after deleting patient 1 there is 1 less patient in the table
     * @throws SQLException 
     */
    @Test
    public void ztestDeletePatient() throws SQLException{
        int records = hospitalDAO.deletePatient(1);
        List<Patient> patients = hospitalDAO.findAll();
        
        assertEquals("A patient record was not deleted properly", 4, patients.size());       
    }
    
    /**
     * this tests if the record is deleted by checking if the spot
     * previously held by patient 1 is now empty (id == 1)
     * @throws SQLException 
     */
    @Test
    public void ztestDeletePatient1() throws SQLException{
        int records = hospitalDAO.deletePatient(1);
        Patient patientEmpty = hospitalDAO.findID(1);
        
        assertEquals("This object should now have patientID == -1", -1, patientEmpty.getPatientID());
    }
    
    /**
     * this tests the update method for patient
     * @throws SQLException 
     */
    @Test
    public void testUpdatePatient() throws SQLException{
        Patient patientTest = hospitalDAO.findID(1);
        
        patientTest.setFirstName("Test");
        
        hospitalDAO.update(patientTest);
        Patient patientUpdate = hospitalDAO.findID(1);
        
        assertEquals("Patient update is not succesful", patientTest.getFirstName(), patientUpdate.getFirstName());
        
    }
    
    /**
     * this tests the update method for inpatient
     * @throws SQLException 
     */
    @Test
    public void testUpdateInpatient() throws SQLException{
        Inpatient inpatientTest = hospitalDAO.findID(1).getListOfInpatients().get(0);
        
        inpatientTest.setRoomNumber("Test");
        
        hospitalDAO.update(inpatientTest);
        Inpatient inpatientUpdate = hospitalDAO.findID(1).getListOfInpatients().get(0);
        
        assertEquals("Inpatient update is not succesful", inpatientTest.getRoomNumber(), inpatientUpdate.getRoomNumber());
    }
    
    /**
     * this tests the update method for medication
     * @throws SQLException 
     */
    @Test
    public void testUpdateMedication() throws SQLException{
        Medication medicationTest = hospitalDAO.findID(1).getListOfMedications().get(0);
        
        medicationTest.setMed("Test");
        
        hospitalDAO.update(medicationTest);
        Medication medicationUpdate = hospitalDAO.findID(1).getListOfMedications().get(0);
        
        assertEquals("Medication update is not succesful", medicationTest.getMed(), medicationUpdate.getMed());
    }
    
    /**
     * this tests the update method for surgical
     * @throws SQLException 
     */
    @Test
    public void testUpdateSurgical() throws SQLException{
        Surgical surgicalTest = hospitalDAO.findID(1).getListOfSurgicals().get(0);
        
        surgicalTest.setSurgery("Test");
        
        hospitalDAO.update(surgicalTest);
        Surgical surgicalUpdate = hospitalDAO.findID(1).getListOfSurgicals().get(0);
        
        assertEquals("Medication update is not succesful", surgicalTest.getSurgery(), surgicalUpdate.getSurgery());
    }
    
    @Before
    public void seedDatabase() {
        final String seedDataScript = loadAsString("CreateHospitalTables.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<String>();
        try {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
    
}
